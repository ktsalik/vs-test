# vs-test

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

## Features
* Displays list of images in rows. Columns depend on the width of the screen. Each column is 320 plus 20 pixels width, which is the image size and margin accordingly.

* Displays image information (id and author) on below.

* Shows the original image on a modal. You can navigate using arrow keys (left/right) while the modal is active.

* Displays 5 pages (current page plus 4 closest pages) according to max images count which is 993 (tested manually). Previous and next page buttons are also available. You can also change page using arrow keys (left/right).

* Searches through all seen images. On each page request, results are being stored locally in order to be "searchable" later.

### Notes
* ````Modal```` just shows the image in it's original size instead of an ````iFrame```` of the url, because unsplash.com has same-origin policy.
* Pages count was calculated manually. If API changes and starts sending more items for example, the app won't be able to access them.
* There is a second branch ````fixed-aspect-ratio```` with a different image display approach, but it's not as stable as master.

*Vue Single File Components rocks!*

### Knows issues
* On mobile devices search suggestions that overflow vertically require 2 clicks. One to close the keyboard and one to choose it.
* Modal is scrollable on mobile devices.
* Menu pops out and becomes visible after Modal scroll on mobile devices.