module.exports = {
  root: true,
  env: {
    node: true,
    browser: true
  },
  parserOptions: {
    parser: "babel-eslint",
    sourceType: "module"
  },
  extends: [
    "eslint:recommended",
    "plugin:vue/recommended"
  ],
  rules: {
    "vue/html-closing-bracket-newline": "never",
    "vue/html-self-closing": "never",
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off"
  }
};